import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/models/search_criteria.dart';
import 'package:trivs/screens/index.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:trivs/screens/widgets/apartment_card.dart';
import 'package:trivs/services/demo_data.dart';

void main() {
  testWidgets('Two favorite apartments results in two cards in favorites view',
      (WidgetTester tester) async {
    final demoApartments = DemoApartments();
    demoApartments
        .getBy(SearchCriteria(0, 1000, [1], []))[0]
        .apartmentDescision = ApartmentDescision.LIKED;
    demoApartments
        .getBy(SearchCriteria(0, 1000, [1], []))[1]
        .apartmentDescision = ApartmentDescision.LIKED;
    await tester.pumpWidget(MaterialApp(
        home: ScopedModel<DemoApartments>(
      model: demoApartments,
      child: Favorites(),
    )));

    expect(find.byType(ApartmentCard), findsNWidgets(2));
  });

  testWidgets(
      'Tapping on an apartment card should trigger navigation to apartment details',
      (WidgetTester tester) async {
    final demoApartments = DemoApartments();
    demoApartments
        .getBy(SearchCriteria(0, 1000, [1], []))[0]
        .apartmentDescision = ApartmentDescision.LIKED;
    await tester.pumpWidget(MaterialApp(
        routes: {'/apartment': (context) => Text("navigation done")},
        home: ScopedModel<DemoApartments>(
          model: demoApartments,
          child: Favorites(),
        )));
    await tester.tap(find.byType(ApartmentCard).at(0));
    await tester.pumpAndSettle();
    expect(find.text("navigation done"), findsOneWidget);
  });
}
