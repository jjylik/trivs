import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:trivs/models/apartment.dart';
import 'widgets/apartment_card.dart';
import 'widgets/bottom_tab_navigation.dart';

class PickFavorite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Apartment> apartments = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("Valitse suosikkisi"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.list),
            onPressed: () {
              Navigator.pushNamed(context, "/favorites");
            },
          )
        ],
      ),
      bottomNavigationBar: BottomTabNavigation(),
      body: Container(child: _PickFavoriteCards(apartments: apartments)),
    );
  }
}

class _PickFavoriteCards extends StatelessWidget {
  final List<Apartment> apartments;

  const _PickFavoriteCards({Key key, @required this.apartments})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SwiperController _swiperController = SwiperController();

    return Swiper(
      itemBuilder: (BuildContext context, int index) {
        return ApartmentCard(
          key: ValueKey(index),
          apartment: apartments[index],
          handleActionCallback: (apartment, action) {
            apartment.apartmentDescision = (action == CardAction.LIKE ? ApartmentDescision.LIKED : ApartmentDescision.DISLIKED);
            _swiperController.next();
          },
        );
      },
      itemCount: apartments.length,
      layout: SwiperLayout.DEFAULT,
      loop: false,
      controller: _swiperController,
    );
  }
}
