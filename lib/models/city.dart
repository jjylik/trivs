import 'package:trivs/models/apartment.dart';

class City {
  final double lat;
  final double lng;
  final List<CityFeature> cityFeatures;

  City(this.lat, this.lng, this.cityFeatures);
}

class CityFeature {
  final String name;
  final ApartmentFeature feature;
  final double lat;
  final double lng;

  CityFeature(this.lat, this.lng, this.name, this.feature);
}
