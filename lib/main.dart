import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';
import 'screens/index.dart';
import 'package:trivs/services/demo_data.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return ScopedModel<DemoApartments>(
      model: DemoApartments(),
      child: MaterialApp(
        title: 'Trivs',
        theme: ThemeData(
            primaryColor:
                Colors.lightGreen[500], //Color.fromRGBO(116, 232, 25, 1),
            accentColor: Color.fromRGBO(119, 184, 0, 1)),
        initialRoute: '/',
        routes: {
          '/': (context) => InitialSearch(),
          '/apartment-features': (context) => ApartmentFeatures(),
          '/search-results': (context) => SearchResults(),
          '/pick-favorite': (context) => PickFavorite(),
          '/favorites': (context) => Favorites(),
          '/apartment': (context) => ApartmentDetails()
        },
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'),
          const Locale('fi'),
        ],
      ),
    );
  }
}
