import 'package:flutter/material.dart';
import 'package:trivs/models/apartment.dart';
import 'widgets/apartment_card.dart';
import 'widgets/bottom_tab_navigation.dart';

class ApartmentDetails extends StatelessWidget {
  ApartmentDetails({Key key});

  @override
  Widget build(BuildContext context) {
    Apartment apartment = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("Asunnon tiedot")
      ),
      bottomNavigationBar: BottomTabNavigation(),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/background.jpg"),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.08), BlendMode.dstATop),
              fit: BoxFit.cover,
            ),
          ),
          child: ApartmentCard(apartment: apartment, showActions: false,)),
    );
  }
}
