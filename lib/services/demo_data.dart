import 'package:scoped_model/scoped_model.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/models/city.dart';
import 'package:trivs/models/search_criteria.dart';

class DemoApartments extends Model {
  List<Apartment> _apartments = _demoApartments();

  List<Apartment> getBy(SearchCriteria searchCriteria) {
    return _apartments
        .where((apartment) =>
            apartment.rent >= searchCriteria.rentMin &&
            apartment.rent <= searchCriteria.rentMax &&
            searchCriteria.numberOfRooms.any((numberOfRooms) => (numberOfRooms < 99 //if 4 or more rooms...
                ? numberOfRooms == apartment.numberOfRooms
                : apartment.numberOfRooms > 3)) &&
            (searchCriteria.apartmentFeatures.length > 0
                ? searchCriteria.apartmentFeatures
                    .any((f) => apartment.apartmentFeatures.contains(f))
                : true))
        .toList();
  }

  get favorites => _apartments
      .where((apt) => apt.apartmentDescision == ApartmentDescision.LIKED).toList();
}


City cityTurku() {
  final turkuFeatures = [
    CityFeature(60.446235,  22.3016936, "Elixia Trivium", ApartmentFeature("Urheilu", null)),
    CityFeature(60.4464864, 22.2929227, "Kupittaan urheiluhalli" , ApartmentFeature("Urheilu", null)),
    CityFeature(60.4531124, 22.2625065,  "P&P Fitness center", ApartmentFeature("Urheilu", null)),
    CityFeature(60.4482022, 22.2497177,  "Studio Move", ApartmentFeature("Urheilu", null)),
    CityFeature(60.4514377, 22.2749412, "Kirjasto", ApartmentFeature("Kulttuuri", null)),
    CityFeature(60.4483687, 22.2797906,  "Turun kesäteatteri", ApartmentFeature("Kulttuuri", null))
  ];
  final turku = City(60.4518, 22.2666, turkuFeatures);
  return turku;
}

List<Apartment> _demoApartments() {
  return List.unmodifiable([
    Apartment(
        "Skanssinkatu 24",
        Neighbourhood("Skanssi, Turku", 60.426348, 22.325028),
        ["images/apartments/skanssi.jpeg", "images/apartments/skanssi2.jpeg"],
        [
          ApartmentFeature("Moderni", "images/modern.svg"),
          ApartmentFeature("Rauhallinen", "images/peaceful.svg"),
          ApartmentFeature("Perhe", "images/family.svg")
        ],
        "Uusi yksiö modernilla alueella",
        540,
        37.5,
        1),
    Apartment(
        "Puutarhakatu 46 E",
         Neighbourhood("Port Arthur, Turku", 60.447485, 22.242010),
        ["images/apartments/b1.jpeg", "images/apartments/b2.jpeg"],
        [
          ApartmentFeature("Puutaloalue", "images/puutaloalue.svg"),
          ApartmentFeature("Moderni", "images/modern.svg"),
          ApartmentFeature("Kulttuuri", "images/culture.svg")
        ],
        "Ylimmän kerroksen remontoitu kahden huoneen koti, jossa kompakti ja toimiva pohjaratkaisu.",
        625,
        49,
        1),
    Apartment(
        "Puutarhakatu 46 A",
         Neighbourhood("Port Arthur, Turku", 60.447485, 22.242010),
        ["images/apartments/c1.jpeg", "images/apartments/c2.jpeg"],
        [
          ApartmentFeature("Puutaloalue", "images/puutaloalue.svg"),
          ApartmentFeature("Urbaani", "images/modern.svg"),
          ApartmentFeature("Kulttuuri", "images/culture.svg"),
          ApartmentFeature("Lemmikit", "images/pets.svg")
        ],
        "Kahden korttelin matka torille.",
        460,
        29,
        1),
    Apartment(
        "Palomäenkatu 25",
        Neighbourhood("Nummi, Turku", 60.455639, 22.297672),
        ["images/apartments/j1.jpeg", "images/apartments/j2.jpeg"],
        [
          ApartmentFeature("Paikallisliikenne", "images/puutaloalue.svg"),
          ApartmentFeature("Rauhalinen", "images/peaceful.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
        ],
        "Yksiö luonnon lähellä",
        390,
        21.5,
        1),
    Apartment(
        "Puutarhakatu 33",
        Neighbourhood("Port Arthur, Turku", 60.447485, 22.242010),
        ["images/apartments/d1.jpeg", "images/apartments/d2.jpeg"],
        [
          ApartmentFeature("Puutaloalue", "images/puutaloalue.svg"),
          ApartmentFeature("Paikallisliikenne", "images/commute.svg"),
          ApartmentFeature("Kulttuuri", "images/culture.svg"),
          ApartmentFeature("Lemmikit", "images/pets.svg")
        ],
        "Tehoneliöinen kaksio todella hyvällä paikalla keskustassa.",
        840,
        60,
        2),
    Apartment(
        "Kaivokatu 16 b",
         Neighbourhood("I kaupunginosa, Turku",60.454399, 22.282389),
        ["images/apartments/e1.jpeg", "images/apartments/e2.jpeg"],
        [
          ApartmentFeature("Urheilu", "images/sports.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
          ApartmentFeature("Kansainvälinen", "images/international.svg"),
        ],
        "Haasteista pitäville hyvä kohde!",
        690,
        44,
        1),
    Apartment(
        "Kaivokatu 16 b",
        Neighbourhood("I kaupunginosa, Turku",60.454399, 22.282389),
        ["images/apartments/f1.jpeg", "images/apartments/f2.jpeg"],
        [
          ApartmentFeature("Urheilu", "images/sports.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
          ApartmentFeature("Kansainvälinen", "images/international.svg"),
        ],
        "Tässä kodissa on kätevä avokeittiö, joka muodostaa olohuoneen kanssa mahtavan valoisan ja korkean tilan, josta on käynti isolle rauhalliselle terassille.",
        690,
        44,
        1),
    Apartment(
        "Tahkonkuja 1 F",
        Neighbourhood("Kupittaa, Turku",60.449125, 22.292213),
        ["images/apartments/g1.jpeg", "images/apartments/g2.jpeg"],
        [
          ApartmentFeature("Kaukoliikenne", "images/train.svg"),
          ApartmentFeature("Paikallisliikenne", "images/commute.svg"),
        ],
        "Ihastuttava, uniikki pikkukoti kekseliäin pikkuratkaisuin",
        720,
        40.5,
        1),
    Apartment(
        "Yliopistonkatu 2",
         Neighbourhood("VI kaupunginosa, Turku",60.454451, 22.273035),
        ["images/apartments/h1.jpeg", "images/apartments/h1.jpeg"],
        [
          ApartmentFeature("Paikallisliikenne", "images/commute.svg"),
          ApartmentFeature("Urheilu", "images/sports.svg"),
        ],
        "Siistikuntoinen yksiö kerrostalon neljännessä kerroksessa aivan ydinkeskustan tuntumassa.",
        420,
        26,
        1),
    Apartment(
        "Käsityöläiskatu 16",
         Neighbourhood("Keskusta, Turku",60.449873, 22.257508),
        ["images/apartments/i1.jpeg", "images/apartments/i2.jpeg"],
        [
          ApartmentFeature("Paikallisliikenne", "images/commute.svg"),
          ApartmentFeature("Urheilu", "images/sports.svg"),
        ],
        "Siisti yksiö hyvien liikuntamahdollisuuksien äärellä.",
        630,
        35,
        1),
    Apartment(
        "Palomäenkatu 25",
        Neighbourhood("Nummi, Turku", 60.455639, 22.297672),
        ["images/apartments/j1.jpeg", "images/apartments/j2.jpeg"],
        [
          ApartmentFeature("Paikallisliikenne", "images/puutaloalue.svg"),
          ApartmentFeature("Rauhalinen", "images/peaceful.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
        ],
        "Siisti yksiö makuualkovilla",
        390,
        21.5,
        1),
    Apartment(
        "Elinantie 8",
        Neighbourhood("Nummi - Kurala, Turku",60.458027, 22.319213),
        ["images/apartments/k1.jpeg", "images/apartments/k2.jpeg"],
        [
          ApartmentFeature("Paikallisliikenne", "images/puutaloalue.svg"),
          ApartmentFeature("Rauhalinen", "images/peaceful.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
        ],
        "",
        625,
        53,
        2),
    Apartment(
        "Skanssinkatu 34 B",
        Neighbourhood("Skanssi, Turku", 60.426348, 22.325028),
        ["images/apartments/a1.jpeg", "images/apartments/a2.jpeg"],
        [
          ApartmentFeature("Moderni", "images/modern.svg"),
          ApartmentFeature("Rauhallinen", "images/peaceful.svg"),
          ApartmentFeature("Perhe", "images/family.svg")
        ],
        "Tilava yksiö sisäpihan puolelta!",
        585,
        29.5,
        1),
    Apartment(
        "Nisse Kavonkatu 6",
        Neighbourhood("Varissuo, Turku",60.446082, 22.356419),
        ["images/apartments/l1.jpeg", "images/apartments/l2.jpeg"],
        [
          ApartmentFeature("Monikulttuurinen", "images/international.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
        ],
        "Tehdasmiljöön henkeen suunniteltu punatiilinen asuinkortteli.",
        625,
        62,
        2),
    Apartment(
        "Kraatarinkatu 3",
        Neighbourhood("Varissuo, Turku",60.446082, 22.356419),
        ["images/apartments/m1.jpeg", "images/apartments/m2.jpeg"],
        [
          ApartmentFeature("Monikulttuurinen", "images/international.svg"),
          ApartmentFeature("Luonto", "images/nature.svg"),
        ],
        "3/6 kerroksen supervaloisa ja erittäin siistikuntoinen yksiö avokeittiöllä sijaitsee vain parin korttelin päässä Turun ydinkeskustasta.",
        490,
        30,
        1)
  ]);
}
