import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:trivs/models/search_criteria.dart';
import 'package:trivs/screens/initial_search.dart';

void main() {
  testWidgets(
      'Initial search with 4+ rooms selected should pass the search criteria to next screen',
      (WidgetTester tester) async {
    final List<SearchCriteria> arguments = <SearchCriteria>[];

    await tester.pumpWidget(MaterialApp(
      home: InitialSearch(),
      onGenerateRoute: (RouteSettings settings) {
        arguments.add(settings.arguments);
        return MaterialPageRoute<void>(
            settings: settings,
            builder: (BuildContext context) => Text("navigation done"));
      },
    ));

    await tester.tap(find.text("4+"));
    await tester.tap(find.byType(FloatingActionButton));
    await tester.pumpAndSettle();
    expect(arguments.single.numberOfRooms.contains(999), true);
  });
}
