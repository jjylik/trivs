import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/screens/widgets/apartment_card.dart';

void main() {

  testWidgets('Apartment card should contain all apartment data', (WidgetTester tester) async {
    await tester.pumpWidget(MaterialApp(
      home: ApartmentCard(apartment: Apartment("address", Neighbourhood("neighbourhood",0,0), ["images/apartments/a1.jpeg"], [], "description", 100, 10.5, 1),),
    ));
    expect(find.text('address'), findsOneWidget);
    expect(find.text('neighbourhood'), findsOneWidget);
    expect(find.text('description'), findsOneWidget);
    expect(find.text('100€'), findsOneWidget);
    expect(find.text('10,5m²'), findsOneWidget);
  });
}