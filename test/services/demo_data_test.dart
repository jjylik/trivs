import 'package:test/test.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/models/search_criteria.dart';
import 'package:trivs/services/demo_data.dart';

void main() {
  group('Should be filtered by search criteria', () {
    final demoApartments = DemoApartments();

    test('number of rooms', ()  =>
      expect(demoApartments.getBy(SearchCriteria(0.0, 1000.0, [1], [])).length, 12));
    test('rent', ()  =>
      expect(demoApartments.getBy(SearchCriteria(400, 500, [1], [])).length, 3));
    test('features', ()  =>
      expect(demoApartments.getBy(SearchCriteria(0, 1000, [1], [ApartmentFeature("Kulttuuri", null)])).length, 2));
    test('all criteria', ()  =>
      expect(demoApartments.getBy(SearchCriteria(500, 1000, [1, 2], [ApartmentFeature("Luonto", null)])).length, 4));
  
});
}
