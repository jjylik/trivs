import 'package:trivs/models/apartment.dart';

class SearchCriteria {
  final double rentMax;
  final double rentMin;
  final List<int> numberOfRooms;
  final List<ApartmentFeature> apartmentFeatures;
  
  SearchCriteria(
      this.rentMin,
      this.rentMax,
      this.numberOfRooms,
      this.apartmentFeatures,
      );


  static SearchCriteria defaultCriteria() {
    return SearchCriteria(100, 1000, [1], []);
  }
}

