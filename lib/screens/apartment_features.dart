import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/models/search_criteria.dart';

import 'widgets/bottom_tab_navigation.dart';

class ApartmentFeatures extends StatelessWidget {
  ApartmentFeatures({Key key});

  @override
  Widget build(BuildContext context) {
    SearchCriteria searchCriteria = ModalRoute.of(context).settings.arguments ?? SearchCriteria.defaultCriteria();
    return Scaffold(
      appBar: AppBar(
        title: Text("Asunto ja asuinalue"),
      ),
      bottomNavigationBar: BottomTabNavigation(),
      body: Container(
          decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background.jpg"),
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.08), BlendMode.dstATop),
            fit: BoxFit.cover,
          ),
        ),
          child: _ApartmentFeaturesList(
            changed: (features) => searchCriteria = SearchCriteria(
                searchCriteria.rentMin, searchCriteria.rentMax, searchCriteria.numberOfRooms, features),
          )),
      floatingActionButton: FloatingActionButton.extended(
        label: Text("Jatka".toUpperCase()),
        onPressed: () {
          Navigator.pushNamed(context, '/search-results', arguments: searchCriteria);
        },
        
      ), 
    );
  }
}

typedef void ApartmentFeaturesChanged(List<ApartmentFeature> features);

class _ApartmentFeaturesList extends StatefulWidget {
  final ApartmentFeaturesChanged changed;

  const _ApartmentFeaturesList({Key key, this.changed}) : super(key: key);

  @override
  _ApartmentFeaturesListState createState() {
    return _ApartmentFeaturesListState();
  }
}

class _ApartmentFeaturesListState extends State<_ApartmentFeaturesList> {
  List<ApartmentFeature> _selectedFeatures = [];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: _buildGridView( context ),
    );
  }

  Column _buildGridView( BuildContext context ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
         Padding(
           padding: const EdgeInsets.only(left: 8.0),
           child: Text("Asunto", style: Theme.of(context).textTheme.headline,),
         ),
         Expanded(
           flex: 1,
           child: GridView.count(crossAxisCount: 3, children: [
            _ApartmentFeaturesListItem(
                assetName: "pets.svg",
                label: "Lemmikit",
                onSelected: _selectOrUnselect("Lemmikit")),
            _ApartmentFeaturesListItem(
                assetName: "parking.svg",
                label: "Autopaikka",
                onSelected: _selectOrUnselect("Autopaikka")),
            _ApartmentFeaturesListItem(
                assetName: "pesutupa.svg",
                label: "Pesutupa",
                onSelected: _selectOrUnselect("Pesutupa")),
           ]),
         ),
       Padding(
           padding: const EdgeInsets.only(left: 8.0),
           child: Text("Asuinalue", style: Theme.of(context).textTheme.headline,),
         ),
        Expanded(
          flex: 3,
          child: GridView.count(crossAxisCount: 3, children: [
            _ApartmentFeaturesListItem(
                assetName: "train.svg",
                label: "Kaukoliikenne",
                onSelected: _selectOrUnselect("Kaukoliikenne")),
            _ApartmentFeaturesListItem(
                assetName: "commute.svg",
                label: "Paikallisliikenne",
                onSelected: _selectOrUnselect("Paikallisliikenne")),
            _ApartmentFeaturesListItem(
                assetName: "family.svg",
                label: "Perhe",
                onSelected: _selectOrUnselect("Perhe")),
            _ApartmentFeaturesListItem(
                assetName: "nature.svg",
                label: "Luonto",
                onSelected: _selectOrUnselect("Luonto")),
            _ApartmentFeaturesListItem(
                assetName: "peaceful.svg",
                label: "Rauhallinen",
                onSelected: _selectOrUnselect("Rauhallinen")),
            _ApartmentFeaturesListItem(
                assetName: "modern.svg",
                label: "Moderni",
                onSelected: _selectOrUnselect("Moderni")),
            _ApartmentFeaturesListItem(
                assetName: "puutaloalue.svg",
                label: "Puutaloalue",
                onSelected: _selectOrUnselect("Puutaloalue")),
            _ApartmentFeaturesListItem(
                assetName: "sports.svg",
                label: "Urheilu",
                onSelected: _selectOrUnselect("Urheilu")),
            _ApartmentFeaturesListItem(
                assetName: "culture.svg",
                label: "Kulttuuri",
                onSelected: _selectOrUnselect("Kulttuuri")),
            _ApartmentFeaturesListItem(
                assetName: "international.svg",
                label: "Kansainvälinen",
                onSelected: _selectOrUnselect("Kansainvälinen")),
          ]),
        ),
      ],
    );
  }

  _selectOrUnselect(String feature) {
    return (selected) {
      ApartmentFeature f = ApartmentFeature(feature, null);
      if (selected) {
        _selectedFeatures.add(f);
      } else {
        _selectedFeatures.remove(f);
      }
      widget.changed(_selectedFeatures);
    };
  }
}

class _ApartmentFeaturesListItem extends StatefulWidget {
  final String assetName;
  final String label;
  final ValueChanged<bool> onSelected;
  const _ApartmentFeaturesListItem(
      {Key key, this.assetName, this.label, this.onSelected})
      : super(key: key);

  @override
  _ApartmentFeaturesListItemState createState() {
    return _ApartmentFeaturesListItemState();
  }
}

class _ApartmentFeaturesListItemState
    extends State<_ApartmentFeaturesListItem> {
  bool _selected = false;

  @override
  Widget build(BuildContext context) {
    double imageWidth = min((MediaQuery.of(context).size.width / 3.0) - 20, 60);
    double imageHeight = min(MediaQuery.of(context).size.height / 4.0, 60);
    Container svgImage = Container(
      color: _selected ? Theme.of(context).accentColor : Colors.transparent,
      child: Column(mainAxisSize: MainAxisSize.max, children: [
        Padding(
          padding: const EdgeInsets.all(4.0),
          child: SvgPicture.asset(
            "images/${widget.assetName}",
            fit: BoxFit.scaleDown,
            width: imageWidth,
            height: imageHeight,
          ),
        ),
        Text(
          widget.label,
        ),
      ]),
    );
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: InkWell(
          onTap: () {
            setState(() {
              _selected = !_selected;
              widget.onSelected(_selected);
            });
          },
          child: svgImage),
    );
  }
}
