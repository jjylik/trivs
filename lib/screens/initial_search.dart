import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:trivs/models/search_criteria.dart';
import 'widgets/bottom_tab_navigation.dart';
import 'dart:async';

class InitialSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SearchCriteria _searchCriteria = SearchCriteria.defaultCriteria();
    var searchCriteriaForm =
        SearchCriteriaForm(changed: (_s) => _searchCriteria = _s);
    return Scaffold(
      appBar: AppBar(
        title: Text("Millaista asuntoa etsit?"),
      ),
      bottomNavigationBar: BottomTabNavigation(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pushNamed(context, '/apartment-features',
              arguments: _searchCriteria);
        },
        label: Text("Jatka".toUpperCase()),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/background.jpg"),
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.08), BlendMode.dstATop),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: searchCriteriaForm,
        ),
      ),
    );
  }
}

typedef void SearchCriteriaChanged(SearchCriteria searchCriteria);

class SearchCriteriaForm extends StatefulWidget {
  final SearchCriteriaChanged changed;

  const SearchCriteriaForm({Key key, this.changed}) : super(key: key);

  @override
  SearchCriteriaFormState createState() {
    return SearchCriteriaFormState();
  }
}
class SearchCriteriaFormState extends State<SearchCriteriaForm> {
  final _formKey = GlobalKey<FormState>();
  final  _typeAheadController = TextEditingController();
  var _rentRange = const RangeValues(100, 1000);
  var _fromDate = DateTime.now();
  var _searchCriteria = SearchCriteria.defaultCriteria();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 18.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            TypeAheadField(
              textFieldConfiguration: TextFieldConfiguration(
                  autofocus: true,
                  controller: _typeAheadController,
                  decoration: InputDecoration(
                      labelText: "Paikkakunta",
                      labelStyle: TextStyle(fontSize: 20))),
              suggestionsCallback: (pattern) async {
                return ["Turku", "Helsinki", "Tampere"]
                    .where((item) =>
                        item.toLowerCase().startsWith(pattern.toLowerCase()))
                    .toList();
              },
              noItemsFoundBuilder: (_) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Paikkakuntaa ei löydy",
                      style: TextStyle(
                          color: Theme.of(context).disabledColor,
                          fontSize: 18)),
                );
              },
              itemBuilder: (context, suggestion) {
                return ListTile(
                  title: Text(suggestion),
                );
              },
              onSuggestionSelected: (suggestion) {
                this._typeAheadController.text = suggestion;
              },
            ),
            InputDecorator(
              decoration: InputDecoration(
                  labelText: "Vuokra",
                  border: InputBorder.none,
                  labelStyle: TextStyle(fontSize: 20)),
              child: Column(
                children: <Widget>[
                  RangeSlider(
                    values: _rentRange,
                    min: 100,
                    max: 3000,
                    divisions: 58,
                    activeColor: Colors.grey,
                    inactiveColor:
                        Theme.of(context).primaryColor.withOpacity(0.24),
                    labels: RangeLabels('${_rentRange.start.round()}',
                        '${_rentRange.end.round()}'),
                    onChanged: (RangeValues values) {
                      setState(() {
                        _rentRange = values;
                        _searchCriteria = SearchCriteria(
                            values.start,
                            values.end,
                            _searchCriteria.numberOfRooms,
                            _searchCriteria.apartmentFeatures);
                        widget.changed(_searchCriteria);
                      });
                    },
                  ),
                  Text(
                    '${_rentRange.start.round()}€ - '
                    '${_rentRange.end.round()}€',
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
            InputDecorator(
              decoration: InputDecoration(
                  labelText: "Huoneiden lukumäärä",
                  border: InputBorder.none,
                  labelStyle: TextStyle(fontSize: 20)),
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      _NumberOfRoomsItem(
                          label: "1",
                          initialValue: true,
                          valueChanged: (selected) =>
                              _noOfRoomsValueChanged(1, selected)),
                      _NumberOfRoomsItem(
                          label: "2",
                          valueChanged: (selected) =>
                              _noOfRoomsValueChanged(2, selected)),
                      _NumberOfRoomsItem(
                          label: "3",
                          valueChanged: (selected) =>
                              _noOfRoomsValueChanged(3, selected)),
                      _NumberOfRoomsItem(
                          label: "4+",
                          valueChanged: (selected) =>
                              _noOfRoomsValueChanged(999, selected)),
                    ]),
              ),
            ),
            _DateTimePicker(
                labelText: 'Milloin haluaisit muuttaa?',
                selectedDate: _fromDate,
                selectDate: (DateTime date) {
                  setState(() {
                    _fromDate = date;
                  });
                })
          ],
        ),
      ),
    );
  }

  _noOfRoomsValueChanged(int numberOfRooms, bool selected) {
    selected
        ? _searchCriteria.numberOfRooms.add(numberOfRooms)
        : _searchCriteria.numberOfRooms.remove(numberOfRooms);
    var newSearchCriteria = SearchCriteria(
        _searchCriteria.rentMin,
        _searchCriteria.rentMax,
        _searchCriteria.numberOfRooms,
        _searchCriteria.apartmentFeatures);
    this._searchCriteria = newSearchCriteria;
    widget.changed(newSearchCriteria);
  }
}

class _NumberOfRoomsItem extends StatefulWidget {
  final String label;
  final ValueChanged<bool> valueChanged;
  final bool initialValue;

  const _NumberOfRoomsItem(
      {Key key,
      @required this.label,
      this.valueChanged,
      this.initialValue = false})
      : super(key: key);

  @override
  _NumberOfRoomsItemState createState() {
    return _NumberOfRoomsItemState(this.initialValue);
  }
}

class _NumberOfRoomsItemState extends State<_NumberOfRoomsItem> {
  bool _selected = false;

  _NumberOfRoomsItemState(bool initialValue) {
    _selected = initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 40,
        height: 40,
        color: _selected ? Theme.of(context).primaryColor : Colors.white,
        child: InkWell(
          child: Center(child: Text(widget.label)),
          onTap: () {
            setState(() {
              _selected = !_selected;
              widget.valueChanged(_selected);
            });
          },
        ));
  }
}

///DATETIME
///
///
//

class _InputDropdown extends StatelessWidget {
  const _InputDropdown({
    Key key,
    this.child,
    this.labelText,
    this.valueText,
    this.valueStyle,
    this.onPressed,
  }) : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: InputDecorator(
        decoration: InputDecoration(
          labelText: labelText,
        ),
        baseStyle: TextStyle(fontSize: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(valueText, style: valueStyle),
            Icon(
              Icons.calendar_today,
              color: Theme.of(context).brightness == Brightness.light
                  ? Colors.grey.shade700
                  : Colors.white70,
            ),
          ],
        ),
      ),
    );
  }
}

class _DateTimePicker extends StatelessWidget {
  const _DateTimePicker({
    Key key,
    this.labelText,
    this.selectedDate,
    this.selectDate,
    this.selectTime,
  }) : super(key: key);

  final String labelText;
  final DateTime selectedDate;
  final ValueChanged<DateTime> selectDate;
  final ValueChanged<TimeOfDay> selectTime;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) selectDate(picked);
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle valueStyle = Theme.of(context).textTheme.subhead;
    return Container(
      height: 80,
      child: _InputDropdown(
        labelText: labelText,
        valueText: DateFormat.yMMMd('fi').format(selectedDate),
        valueStyle: valueStyle,
        onPressed: () {
          _selectDate(context);
        },
      ),
    );
  }
}
