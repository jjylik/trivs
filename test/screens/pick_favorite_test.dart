import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/screens/index.dart';

void main() {
  testWidgets('Picking favorites should change the apartment descision',
      (WidgetTester tester) async {
    var apartments = [Apartment("address", Neighbourhood("neighbourhood",0,0), ["images/apartments/a1.jpeg"],
              [ApartmentFeature("Moderni", "images/modern.svg"),], "description", 10, 10, 1)];
    await tester.pumpWidget(MaterialApp(
      onGenerateRoute: (RouteSettings settings) {
          return MaterialPageRoute<void>(
            settings: RouteSettings(arguments: apartments),
            builder: (BuildContext context) => PickFavorite(),
          );
      },
    ));
    await tester.pumpAndSettle();
    await tester.tap(find.byType(GestureDetector).at(1));
    await tester.pumpAndSettle();
    expect(ApartmentDescision.LIKED, apartments.first.apartmentDescision);
  }, skip: true); //flaky
}
