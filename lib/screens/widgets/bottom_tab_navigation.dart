import 'package:flutter/material.dart';

class BottomTabNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      child: Container(
        height: 50.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Image.asset("images/home.png",
                width: 45, height: 40, fit: BoxFit.contain),
            GestureDetector(
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
              },
              child: Image.asset("images/search.png",
                  width: 45, height: 40, fit: BoxFit.contain),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/favorites', (_) => false);
              },
              child: Image.asset("images/like.png",
                  width: 45, height: 40, fit: BoxFit.contain),
            ),
            Image.asset("images/notifications.png",
                width: 45, height: 40, fit: BoxFit.contain),
            Image.asset("images/profile.png",
                width: 45, height: 40, fit: BoxFit.contain),
          ],
        ),
      ),
    );
  }
}
