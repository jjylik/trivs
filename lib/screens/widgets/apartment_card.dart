import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:trivs/models/apartment.dart';

typedef void CardActionCallback(Apartment apartment, CardAction action);

class ApartmentCard extends StatelessWidget {
  final CardActionCallback handleActionCallback;
  final bool small;
  final Apartment apartment;
  final showActions;

  const ApartmentCard(
      {Key key,
      this.handleActionCallback,
      this.small = false,
      this.showActions = true,
      @required this.apartment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            _buildTitle(small, textTheme),
            Align(
                alignment: Alignment.topLeft,
                child: Text(
                  apartment.neighbourhood.name,
                  style: apartment.neighbourhood.name.length < 20
                      ? textTheme.subhead
                      : textTheme.body1,
                )),
            Stack(
              children: <Widget>[
                Container(
                  height: !small
                      ? MediaQuery.of(context).size.height * 0.4
                      : MediaQuery.of(context).size.height * 0.1,
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: apartment.imageAssetPaths
                          .map<Widget>((imagePath) => Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  imagePath,
                                  height: !small ? 300 : 100,
                                ),
                              ))
                          .toList()),
                ),
                Align(
                    alignment: Alignment.topRight,
                    child: Icon(
                      Icons.collections,
                      size: 35,
                      color: Theme.of(context).highlightColor,
                    ))
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("${apartment.rent.round().toString()}€",
                      style: !small ? textTheme.headline : textTheme.subhead),
                  Text("${apartment.size.toString().replaceAll(".", ",")}m²",
                      style: !small ? textTheme.headline : textTheme.subhead)
                ],
              ),
            ),
            Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: apartment.apartmentFeatures
                    .map<Widget>((feature) =>
                        _buildApartmentFeatureIcon(feature.imageAssetPath))
                    .toList()),
            if (!small) ..._buildActionsAndDescription()
          ],
        ),
      ),
    );
  }

  List<Widget> _buildActionsAndDescription() {
    return [
      Padding(
        padding: EdgeInsets.only(top: 8),
        child: Align(
            alignment: Alignment.topLeft,
            child: Text(this.apartment.description)),
      ),
      if (showActions)
        Expanded(
            key: ValueKey(apartment),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _LikeOrDislikeItem(
                    action: CardAction.DISLIKE,
                    apartment: apartment,
                    initialValue: apartment.apartmentDescision ==
                        ApartmentDescision.DISLIKED,
                    handleAction: handleActionCallback),
                _LikeOrDislikeItem(
                    action: CardAction.LIKE,
                    initialValue: apartment.apartmentDescision ==
                        ApartmentDescision.LIKED,
                    apartment: apartment,
                    handleAction: handleActionCallback)
              ],
            ))
    ];
  }

  _buildApartmentFeatureIcon(String assetName) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: SvgPicture.asset(
        assetName,
        width: 30,
        height: 30,
      ),
    );
  }

  _buildTitle(small, TextTheme textTheme) {
    if (!small) {
      return Align(
          alignment: Alignment.topLeft,
          child: Text(apartment.address, style: textTheme.title));
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Align(alignment: Alignment.topLeft, child: Text(apartment.address)),
          InkWell(
              onTap: () =>
                  handleActionCallback(this.apartment, CardAction.DISLIKE),
              child: Image.asset(
                "images/like-light-selected.png",
                width: 20,
              ))
        ],
      );
    }
  }
}

class _LikeOrDislikeItem extends StatefulWidget {
  final CardAction action;
  final Apartment apartment;
  final CardActionCallback handleAction;
  final bool initialValue;

  const _LikeOrDislikeItem(
      {Key key,
      @required this.action,
      @required this.apartment,
      this.handleAction,
      @required this.initialValue})
      : super(key: key);

  @override
  _LikeOrDislikeItemState createState() {
    return _LikeOrDislikeItemState(this.initialValue);
  }
}

class _LikeOrDislikeItemState extends State<_LikeOrDislikeItem> {
  bool _selected = false;

  _LikeOrDislikeItemState(initialValue) {
    _selected = initialValue;
  }

  @override
  Widget build(BuildContext context) {
    String assetType =
        widget.action == CardAction.LIKE ? "like-light" : "discard";
    String assetName =
        _selected ? "images/$assetType-selected.png" : "images/$assetType.png";
    return GestureDetector(
        onTap: () {
          setState(() {
            _selected = !_selected;
            widget.handleAction(widget.apartment, widget.action);
          });
        },
        child: Image.asset(
          assetName,
          width: 60,
          fit: BoxFit.scaleDown,
        ));
  }
}

enum CardAction { LIKE, DISLIKE }
