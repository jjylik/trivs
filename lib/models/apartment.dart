class Apartment {
  final String address;
  final Neighbourhood neighbourhood;
  final List<String> imageAssetPaths;
  final List<ApartmentFeature> apartmentFeatures;
  final double rent;
  final double size;
  final int numberOfRooms;
  final String description;
  ApartmentDescision apartmentDescision;

  Apartment(this.address, this.neighbourhood, this.imageAssetPaths,
      this.apartmentFeatures, this.description, this.rent, this.size, this.numberOfRooms);
}

class ApartmentFeature {
  final String label;
  final String imageAssetPath;

  ApartmentFeature(this.label, this.imageAssetPath);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ApartmentFeature &&
          runtimeType == other.runtimeType &&
          label == other.label;

  int get hashCode => label.hashCode;
}

class Neighbourhood {

  final String name;
  final double lat;
  final double lng;

  Neighbourhood(this.name, this.lat, this.lng);

  @override
  bool operator ==(Object other)  =>
     identical(this, other) ||
      (other is Neighbourhood &&
          runtimeType == other.runtimeType &&
          name == other.name);


  int get hashCode => name.hashCode;

}

enum ApartmentDescision {
  LIKED, DISLIKED
}
