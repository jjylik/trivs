import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/models/search_criteria.dart';
import 'package:trivs/screens/widgets/bottom_tab_navigation.dart';
import 'package:trivs/services/demo_data.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class SearchResults extends StatelessWidget {
  SearchResults({Key key});

  @override
  Widget build(BuildContext context) {
    SearchCriteria searchCriteria = ModalRoute.of(context).settings.arguments;
    final foundApartments =
        ScopedModel.of<DemoApartments>(context).getBy(searchCriteria);
    return foundApartments.length > 0
        ? _buildSearchResults(
            context, foundApartments, searchCriteria.apartmentFeatures)
        : _buildNoApartmentsFound(context);
  }

  _buildSearchResults(BuildContext context, List<Apartment> foundApartments,
      List<ApartmentFeature> searchCriteriaFeatures) {
    Set<Neighbourhood> topNeighbourhoods = foundApartments
        .map<Neighbourhood>((apt) => apt.neighbourhood)
        .toSet()
        .take(3)
        .toSet();

    return Scaffold(
      appBar: AppBar(
        title: Text("Hakutulokset"),
      ),
      bottomNavigationBar: BottomTabNavigation(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pushNamed(context, '/pick-favorite',
              arguments: foundApartments);
        },
        label: Text("Näytä".toUpperCase()),
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/background.jpg"),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.08), BlendMode.dstATop),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: _NeighbourhoodMap(
                      topNeighbourhoods, searchCriteriaFeatures)),
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Theme.of(context).primaryColor,
                      child: Padding(
                          padding: EdgeInsets.all(8),
                          child: Center(
                              child: Text(
                            "Hakutuloksia yhteensä ${foundApartments.length} kpl",
                            style: Theme.of(context).textTheme.title,
                          ))),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Text("Sinulle sopivimmat asuinalueet",
                          style: Theme.of(context).textTheme.subhead),
                    ),
                    ...topNeighbourhoods.map<Widget>((neighbourhood) =>
                        _buildLocationTile(context, neighbourhood.name))
                  ],
                ),
              ),
            ],
          )),
    );
  }

  _buildNoApartmentsFound(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hakutulokset"),
      ),
      bottomNavigationBar: BottomTabNavigation(),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
        },
        label: Text("Hae uudelleen".toUpperCase()),
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/background.jpg"),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.08), BlendMode.dstATop),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child: Container(
              child: Padding(
                  padding: EdgeInsets.all(8),
                  child: Center(
                      child: Text(
                    "Asuntoja ei löytynyt, kokeile erilaisilla hakuehdoilla",
                    style: Theme.of(context).textTheme.title,
                  ))),
            ),
          )),
    );
  }

  _buildLocationTile(BuildContext context, String location) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        children: <Widget>[
          Icon(Icons.place, color: Colors.black45),
          Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Text(location, style: Theme.of(context).textTheme.subhead),
          )
        ],
      ),
    );
  }
}

class _NeighbourhoodMap extends StatelessWidget {
  final Set<Marker> markers = Set();

  final CameraPosition _turkuPosition = CameraPosition(
    target: LatLng(cityTurku().lat, cityTurku().lng),
    zoom: 10.4746,
  );

  _NeighbourhoodMap(
      Set<Neighbourhood> neighborhoods, List<ApartmentFeature> features) {
    markers.addAll(neighborhoods.map((n) => Marker(
        markerId: MarkerId(n.name),
        position: LatLng(n.lat, n.lng),
        infoWindow: InfoWindow(title: n.name))));
    final turku = cityTurku();
    markers.addAll(turku.cityFeatures
        .where((f) => features.contains(f.feature))
        .map((f) => _buildMarker(f.lat, f.lng, f.name)));
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        height: 200,
        child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _turkuPosition,
          markers: markers,
        ));
  }

  _buildMarker(double lat, double lng, String name) {
    return Marker(
        markerId: MarkerId(name),
        position: LatLng(lat, lng),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        infoWindow: InfoWindow(title: name));
  }
}
