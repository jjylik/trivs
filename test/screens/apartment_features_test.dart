import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:trivs/models/search_criteria.dart';
import 'package:trivs/screens/index.dart';


void main() {
  testWidgets(
      "All apartment features are shown",
      (WidgetTester tester) async {

    await tester.pumpWidget(MaterialApp(
      home: ApartmentFeatures(),
    ));

    expect(find.byType(InkWell), findsNWidgets(7)); //seven cards
    expect(find.text("Pesutupa"), findsOneWidget); //cards have labels
  });

  testWidgets(
      "Search criteria is updated based on selected features",
      (WidgetTester tester) async {
  
   var searchCriteria = SearchCriteria.defaultCriteria();
   await tester.pumpWidget(MaterialApp(
      home: ApartmentFeatures(),
      onGenerateRoute: (RouteSettings settings) {
        var route = MaterialPageRoute<void>(
            settings: RouteSettings(arguments: SearchCriteria.defaultCriteria()),
            builder: (BuildContext context) => Text("navigation done"));
        searchCriteria = settings.arguments;
        return route;
      },
    ));
    await tester.tap(find.text("Pesutupa"));
    await tester.tap(find.text("Autopaikka"));
    await tester.tap(find.byType(FloatingActionButton));
    await tester.pumpAndSettle();
    expect(2, searchCriteria.apartmentFeatures.length);
  });
}
