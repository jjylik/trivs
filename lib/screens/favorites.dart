import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:trivs/models/apartment.dart';
import 'package:trivs/services/demo_data.dart';
import 'widgets/apartment_card.dart';
import 'widgets/bottom_tab_navigation.dart';

class Favorites extends StatelessWidget {
  Favorites({Key key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Suosikit"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.sort),
            onPressed: () {},
          )
        ],
      ),
      bottomNavigationBar: BottomTabNavigation(),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/background.jpg"),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.08), BlendMode.dstATop),
              fit: BoxFit.cover,
            ),
          ),
          child: _FavoritesList(initialApartments: ScopedModel.of<DemoApartments>(context).favorites)),
    );
  }
}

class _FavoritesList extends StatefulWidget {

  final initialApartments;

  const _FavoritesList({Key key, this.initialApartments}) : super(key: key);

  @override
  _FavoritesListState createState() {
    return _FavoritesListState(initialApartments);
  }
}

class _FavoritesListState extends State<_FavoritesList> {
  List<Apartment> apartments;

  _FavoritesListState(this.apartments);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        crossAxisCount: 2,
        childAspectRatio: 7 / 8,
        children: apartments
            .map<Widget>((a) => InkWell(
                onTap: () =>
                    Navigator.pushNamed(context, '/apartment', arguments: a),
                child: ApartmentCard(
                  small: true,
                  showActions: false,
                  apartment: a,
                  handleActionCallback: (apartment, action) {
                    setState(() {
                      apartment.apartmentDescision = (action == CardAction.LIKE
                          ? ApartmentDescision.LIKED
                          : ApartmentDescision.DISLIKED);
                      this.apartments.remove(apartment);
                    });
                  },
                )))
            .toList());
  }
}
